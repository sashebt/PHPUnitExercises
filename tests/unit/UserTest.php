<?php

class UserTest extends \PHPUnit\Framework\TestCase

{
	public function setUp()
	{
		$this->user = new \App\Models\User;
	}


	/** @test */

	public function that_than_we_can_get_first_name()

	{

		$this->user->setFirstName('Aleksandar');
		

		$this->assertEquals($this->user->getFirstName(), "Aleksandar");

	}

	/** @test */
	
	public function that_we_can_get_last_name()
	{
		$this->user->setLastName('Trajkovski');

		$this->assertEquals($this->user->getLastName(), "Trajkovski");
	}

	public function testFullNameIsReturned()
	{
		
		$this->user->setFirstName("Sase");
		$this->user->setLastName("Traj");

		$this->assertEquals($this->user->getFullName(), "Sase Traj");
	}

	public function testFirstAndLastNameAreTrimmed()
	{
	
		$this->user->setFirstName('  Goran    ');
		$this->user->setLastName(' Milosevski      ');

		$this->assertEquals($this->user->getFirstName(), "Goran");
		$this->assertEquals($this->user->getLastName(), "Milosevski");
	}

	public function testEmailAddressCanBeSet()
	{
	

		$this->user->setEmail('at91bt@gmail.com');

		$this->assertEquals($this->user->getEmail(), "at91bt@gmail.com");

	}


	public function testEmailVariablesContainCorrectValues()
	{
		
		$this->user->setFirstName('Goran');
		$this->user->setLastName('Milosevski');
		$this->user->setEmail('goran@gmail.com');

		$emailVariables = $this->user->getEmailVariables();

		$this->assertArrayHasKey('full_name', $emailVariables);
		$this->assertArrayHasKey('email', $emailVariables);

		$this->assertEquals($emailVariables['full_name'], "Goran Milosevski");
		$this->assertEquals($emailVariables['email'], "goran@gmail.com");

	}
}